Name: ${PROJECT_NAME}
Description: The spglib library
Version: ${PROJECT_VERSION}
Libs: -L${CMAKE_INSTALL_PREFIX}/lib -lsymspg
Cflags: -I${CMAKE_INSTALL_PREFIX}/include
